# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Chat app]
* Key functions (add/delete)
    1. [可一對一聊天]
    2. [可加載歷史訊息(目前無上限)]
    3. [即時接收到對方傳送的訊息]
    4. [可與新/舊用戶聊天]
* Other functions (add/delete)
    1. [可上傳頭貼]
    2. [可改名字]
    3. [即時更改對方的顯示的狀態(對方頭貼的更換&姓名的改變)]
    4. [若在登入狀態且有除了你正在聊天的對象傳訊息給你的話，會通知你有訊息]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
1. 若在未登入的狀況下拜訪 index.html ，會直接被踢出去signin.html
2. signin.html其實就是直接拿Lab6來用，再多加了一個Forget password而已
3. 先在email address欄打好郵件地址，再按Forget password就會寄出密碼更改信
4. 首次登入完成後，若你是用google登入，則預設姓名為google的名字，否則為email address'@'前的字串)
5. 首次登入完成後，若你是用google登入，則預設圖片為google的圖片，否則為一張網路上的圖片
6. 點擊左側的navbar時，若視窗寬>767.98，由左向右滑出的框框會將原網頁內容往右擠;否則由左向右滑出的框框會直接蓋過原網頁內容，而原內容不動(這裡用了css animation)
7. 姓名和圖片都可以在Account裡更改，更改的同時與你聊天的人也會同時看到改動
8. 左側的message點開後可以跟你的朋友們聊天，預設為每個人都是每個人的朋友，而更改朋友性質的功能沒來得及做出來，所以就...
9. 點開聊天頁面並發送訊息後，理所當然地，訊息框的頭貼、姓名會是發送者的頭貼和姓名，並會在對方或己方更改頭貼、姓名時即時更改圖示和文字
10. 聊天頁面下方的訊息欄會將enter當斷行，並在送出後呈現在訊息框中
11. 若有存有對話紀錄的對象傳送訊息給你，而你正處於登入狀態，且並非正在與該對象聊天，則會跳Notification告知你該訊息

## Security Report (optional)
訊息的存法為：若A傳訊息給B，資料會push到 message/A的uid/B的uid 和 message/B的uid/A的uid 裡面，而rule設為：
    "message":{
      "$user":{
        "$touser":{
          ".write": "$user==auth.uid||$touser==auth.uid",
          ".read": "$user==auth.uid"  
        }
      }
    }
就只有他們兩個可以寫，而read更只有他本人可以看(不過我覺得給另外一個人看其實也沒差)

個資的存法則是分為public和private兩種，不過我目前這兩大類只有存 name、email、photo 而已，
都存在public裡面，就沒有建private了。
另外我在剛開始做Midterm project的時候有想要做封鎖、隱藏和群組之類的，所以有一個touser和一個group都放在個資裡，
touser裡面存放'某人是不是你的朋友'，group則是打算存放群組名稱，但都沒時間做，所以每個人都是每個人的朋友，
然後也沒有建group資料夾。rule設為：
    "users":{
      "$uid":{
        "public":{
          ".read":"auth!=null",
          ".write":"auth!=null && $uid==auth.uid"
        },
        "$otherdata":{
          ".read":"auth!=null && $uid==auth.uid",
          ".write":"auth!=null && $uid==auth.uid"
        }
      }
    }
如此一來public名符其實大家都能看，不過只有自己能改資料;而public以外的其他的資料都只能自己讀和寫。